#pragma once
#include "CoreMinimal.h"
#include "IPickuperCharacter.generated.h"

UINTERFACE()
class UPickuperCharacter : public UInterface
{
	GENERATED_BODY()
};

class MYPROJECT6_API IPickuperCharacter
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnReachPineapple();
};

