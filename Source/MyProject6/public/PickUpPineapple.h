#pragma once
#include "PickUpActor.h"
#include "PickUpPineapple.generated.h"

UCLASS()
class APickUpPineapple: public APickUpActor
{
	GENERATED_BODY()

public:
	virtual void OnPickedUp(AActor* OtherActor) override;
};
