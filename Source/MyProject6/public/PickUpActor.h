// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickUpActor.generated.h"

UCLASS()
class MYPROJECT6_API APickUpActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APickUpActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Destroyed() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	class USphereComponent* OverlapSphere;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MeshRotationRate = 100;
	UFUNCTION()
	void OnSphereOverlap(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	                     UPrimitiveComponent* OtherPrimitiveComponent, int32 OtherBodyIndex, bool bFromSweep,
	                     const FHitResult& HitResult);
	
	virtual void OnPickedUp(AActor* OtherActor);

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	class UNiagaraSystem* PickupEmitter;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	class USoundCue* PickupSound;
};
