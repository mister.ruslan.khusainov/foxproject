// Fill out your copyright notice in the Description page of Project Settings.


#include "../public/PickUpActor.h"

#include "IPickuperCharacter.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraFunctionLibrary.h"
#include "Sound/SoundCue.h"

// Sets default values
APickUpActor::APickUpActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	OverlapSphere = CreateDefaultSubobject<USphereComponent>(TEXT("OverlapSphere"));
	OverlapSphere->SetupAttachment(RootComponent);
	OverlapSphere->SetSphereRadius(100.f);
	OverlapSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapSphere->SetCollisionResponseToAllChannels(ECR_Ignore);
	OverlapSphere->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(OverlapSphere);
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

}

// Called when the game starts or when spawned
void APickUpActor::BeginPlay()
{
	Super::BeginPlay();
	OverlapSphere->OnComponentBeginOverlap.AddDynamic(this, &APickUpActor::OnSphereOverlap);
}

void APickUpActor::Destroyed()
{
	Super::Destroyed();
	if (PickupEmitter)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, PickupEmitter, GetActorLocation(), GetActorRotation());
	}
	if (PickupSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, PickupSound, GetActorLocation());
	}
}

// Called every frame
void APickUpActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Mesh->AddRelativeRotation(FRotator(0.f, DeltaTime* MeshRotationRate, 0.f));
}

void APickUpActor::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                   UPrimitiveComponent* OtherPrimitiveComponent, int32 OtherBodyIndex, bool bFromSweep,
                                   const FHitResult& HitResult)
{
	if (OtherActor->Implements<UPickuperCharacter>())
	{
		OnPickedUp(OtherActor);
	}
}

void APickUpActor::OnPickedUp(AActor* OtherActor)
{
}
