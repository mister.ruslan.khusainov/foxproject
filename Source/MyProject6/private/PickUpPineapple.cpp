#include "../Public/PickUpPineapple.h"

#include "MyProject6/public/IPickuperCharacter.h"

void APickUpPineapple::OnPickedUp(AActor* OtherActor)
{
	Super::OnPickedUp(OtherActor);
	IPickuperCharacter::Execute_OnReachPineapple(OtherActor);
	Destroy();
}
